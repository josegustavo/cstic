<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Route::group(["middleware" => ["auth:api"]], function () {

$routes_apis = [
	'type',
	'activity',
	'supplier',
	'department',
	'resource_service',
	'feature',
	'keyword',
	'rate',
	'user'
];
foreach ($routes_apis as $name) {
	$class = str_replace('_', '', ucwords($name, '_'));
	Route::delete($name, $class.'Controller@destroyMany');
	//Route::get($name.'/generate',  $class.'Controller@generateFake');
	Route::apiResource($name, $class.'Controller');
}


Route::post('file',  'FileController@upload');
Route::get('tag/most_used',  'TagController@getMostUsed');
Route::get('service/url_card',  'ServiceController@getUrlCard');
Route::get('tag/timing',  'TagController@getTiming');
Route::post('user/import',  'UserController@import');


//});

Auth::routes();