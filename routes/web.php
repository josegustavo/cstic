<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'index');
Route::view('/mis-sugerencias', 'index');
Route::view('/mis-calificaciones', 'index');
Route::view('/buscar/{text?}', 'index');
Route::view('/rs/{slug}', 'index');
Route::view('/rs/{text}/calificaciones', 'index');


Route::group(['middleware' => 'verify.login'], function () {

Route::view('/rs/{text}/editar', 'index');
Route::view('/rs/{text}/calificar', 'index');
Route::view('/apps/{app}', 'index');

});





//Route::get('/home', 'HomeController@index')->name('home');