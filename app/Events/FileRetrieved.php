<?php

namespace App\Events;

use App\Models\File;
use Illuminate\Queue\SerializesModels;

class FileRetrieved
{
    use SerializesModels;

    public $file;

    /**
     * Create a new event instance.
     *
     * @param  Order  $order
     * @return void
     */
    public function __construct(File $file)
    {
        $this->file = $file;
        if($file->url)
        {
            $this->file->url = str_replace("http://d2y0j606jav6ij.cloudfront.net","http://cdn.cstic.club",$this->file->url);
        }
    }
}