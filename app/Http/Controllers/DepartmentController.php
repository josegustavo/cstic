<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DepartmentController extends ApiController
{
    public function show($id)
    {
        $model = static::$model::with('supplier')->findOrFail($id);
        
        return $model;
    }

    public function index()
    {
        return static::$model::with('supplier')->get();        
    }
}
