<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;

class SupplierController extends ApiController
{
    protected function getWith()
    {
        return ['image','departments'];
    }

    public function index()
    {
        $result = parent::index();

        if(Input::has('associative'))
        {
            //dd($result);
            $result = $result->pluck(NULL,'_id');
        }

        return $result;
    }
}