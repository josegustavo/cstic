<?php

namespace App\Http\Controllers;

use Input;

class TagController extends Controller
{
    public function getMostUsed()
    {
        $tags_db = \App\Models\ResourceService::whereNotNull('tags')->get(['tags']);

        $tags = [];
        foreach ($tags_db as $key => $rs) {
        	$tags_rs = $rs['tags'];
        	foreach ($tags_rs as $t => $tag) {
        		if(!isset($tags[$tag]))
        			$tags[$tag] = 1;
        		else
        			$tags[$tag]++;
        	}
        }

        $fsize_min = 80;
        $fsize_max = 125;

        $min_used = 1;
        $max_used = max($tags);

        arsort($tags);
        $tags = array_slice($tags, 0, 20);

        // 1 used => 75%
        // $max_used => 125%
        $result = [];
        foreach ($tags as $key => $count_used) {
        	$result[] = [
        		'tag' => $key,
        		'size' => round($fsize_max - (($fsize_max-$fsize_min)*($max_used-$count_used)/($max_used-$min_used))),
        	];
        }
        
        return $result;
    }

	public function getTiming()
	{
		$time = microtime(true);
  	  	$tags_db = \App\Models\ResourceService::whereNotNull('tags')->get(['tags']);

		dd(microtime(true)-$time);
	}

}