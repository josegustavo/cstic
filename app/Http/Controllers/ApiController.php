<?php

namespace App\Http\Controllers;

use Input;

use Response;

class ApiController extends Controller
{
    protected static $model = null;

    protected function getWith()
    {

    }

    public function __construct()
    {
        self::$model = ('\\App\\Models\\'.str_replace('Controller','',(class_basename(static::class))));
    }

    public function index()
    {
        if($with = $this->getWith())
            $result = static::$model::with($with)->get();
        else
            $result = static::$model::all();    
            
        if(Input::has('associative'))
        {
            //dd($result);
            $result = $result->pluck(NULL,'_id');
        }
        return $result;
    }

    public function store()
    {
        $model = new static::$model;
		if($user = auth()->user())
			$model->created_by = $user->_id;
		
        $save = $this->save($model);
        if($save)
        {
            $result = ['success' => $save?true:false];
            if($save && $model->slug)
                $result['slug'] = $model->slug;
            return $result;
        }
        else
            return Response::json(['message'   =>  "No se pudo crear el recurso."], 401);
    }
 
    public function show($id)
    {
        $model = static::$model::select();
		
        if($with = $this->getWith())
            $model->with($with);
		$result = $model->findOrFail($id);
        return $result;
    }

    public function update($id)
    {
        $model = static::$model::findOrFail($id);
        
        if($user = auth()->user())
        {
            if(!$model->created_by)
            $model->created_by = $user->_id;
            $model->updated_by = $user->_id;
        }
            

        $save = $this->save($model);
        if($save)
        {
            $result = ['success' => $save?true:false];
            if($save && $model->slug)
                $result['slug'] = $model->slug;
            return $result;
        }else
        {
            return Response::json(['message'   =>  "No se pudo modificar este recurso."], 401);
        }
    }

    protected function save(&$model)
    {
        foreach (Input::all() as $key => $value)
            $model->$key = $value;

        $save = $model->save();
        return $save;
    }

    public function destroy($id)
    {
        return ['success' => static::$model::destroy($id)?true:false];
    }

    public function destroyMany()
    {
        $ids = Input::post();
        return ['success' => static::$model::destroy($ids)??false];
    }

    public function generateFake(){
        $count = Input::get('fakedata');
        $result = [];
        if($count < 1000){
            $faker = Faker::create();
            for ($i=1; $i <= $count; $i++) {
                $model = new static::$model;
                $model->name = $faker->word;
                $model->description = $faker->sentence;
                $model->icon = $faker->word;
                $model->save();
                $result []= $model->id;
            }
        }
        return $result;
    }
}