<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;

class FileController extends Controller
{
    public function upload(Request $request)
    {
        $this->validate($request, [
                'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
        ]);
        $image = $request->file('file');

        $md5 = md5_file($image->getPathname());
            

        $file_name =  str_slug(preg_replace("/\.[^.]+$/", "", basename($image->getClientOriginalName()))) . '.' . strtolower($image->
            getClientOriginalExtension());
        

        $model = \App\Models\File::firstOrNew(['md5' => $md5]);
        $model->name = $file_name;
        $model->md5 = $md5;

        if(!$model->id)
            $model->save();
        
        $uniqid = $model->id;
        $base_url = '/images/'.$uniqid.'/'.$file_name;
        $url = config('app.cdn_url').$base_url;
        $model->base_url = $base_url;
        $model->url = $url;
        

        $destinationPath = public_path('/images/'.$uniqid);
        $destinationName = $destinationPath.'/'.$file_name;
        $image->move($destinationPath, $file_name);

        ini_set('memory_limit','128M');
        $new_image = Image::make($destinationName);

        $w = $new_image->width();
        $h = $new_image->height();
        if($new_image->width()>1140)
            $new_image->resize(1140,round($h*1140/$w) )->save($destinationName, 80);
        
        $model->width = $new_image->width();
        $model->height = $new_image->height();

        $model->save();
        
        return $model;
    }

}