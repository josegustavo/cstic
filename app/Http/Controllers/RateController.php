<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;

class RateController extends ApiController
{
    public function index()
    {
        $query = static::$model::with('creator');
        
        
        $rs_id = Input::get('rs_id',0);
        $query->where(['rs_id' => $rs_id]);
        $query->orderBy('updated_at', 'desc');
        return $query->get();   
    }

    public function show($rs_id)
    {
        $user_id = auth()->user()->_id;
        $model = static::$model::select();
		
        if($with = $this->getWith())
            $model->with($with);

        $model->where(['created_by' => $user_id]);
        $model->where(['rs_id' => $rs_id]);
        
        $result = $model->first();
        return $result;
    }

    public function store()
    {
        $model = new static::$model;
		if($user = auth()->user())
			$model->created_by = $user->_id;
		
        $save = $this->save($model);

        \App\Models\ResourceService::calcRate($model->rs_id);

        return ['success' => $save?$model->_id:false];
    }
 
    public function update($id)
    {
        $model = static::$model::findOrFail($id);
        
        if($user = auth()->user())
        {
            if(!$model->created_by)
            $model->created_by = $user->_id;
            $model->updated_by = $user->_id;
        }
        $save = $this->save($model);

        \App\Models\ResourceService::calcRate($model->rs_id);
        
        return ['success' => $save?true:false];
    }

    public function destroy($id)
    {
        $model = static::$model::findOrFail($id);
        $remove = static::$model::destroy($id)?true:false;
        \App\Models\ResourceService::calcRate($model->rs_id);
        return ['success' => $remove];
    }
}
