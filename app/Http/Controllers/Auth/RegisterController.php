<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if(!isset($data['username']))
        {
            $username = explode('@',$data['email']??'');
            $data['username'] = $data['username'] ?? $username[0];
        }
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'username' => 'unique:users|required|string|max:32',
            //'email' => 'required|unique:users|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if(!isset($data['username']))
        {
            $email = $data['email']??'';
            $data['username'] = $data['username'] ?? explode('@',$email);
        }
        $user = User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email']??'',
            'password' => bcrypt($data['password']),
        ]);
        return $user;
    }

    public function register()
    {
        $this->validator(Request::all())->validate();

        event(new Registered($user = $this->create(Request::all())));

        $this->guard()->login($user);

        return $user;
    }
}