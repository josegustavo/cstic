<?php

namespace App\Http\Controllers;

use Input;

class ServiceController extends Controller
{
    public function getUrlCard()
    {
        $url = Input::get('url');
        
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
        throw new \Exception('Url no válida '. $url);

        $agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';

        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt($ch, CURLOPT_URL, $url); 
        $content = curl_exec($ch); 
        curl_close($ch);  
        //$content = @file_get_contents($url);
        //dd($content);
        if(!$content) throw new \Exception('Url no tiene contenido ' .  $url);

        
        
        $doc = new \DOMDocument('1.0', 'UTF-8');
        $doc->recover = true;
        $doc->strictErrorChecking  = FALSE;
        $content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
        @$doc->loadHTML($content);
        

        libxml_use_internal_errors(true);

        $xml = simplexml_import_dom($doc);
        $arr = $xml->xpath('//link[@rel="shortcut icon"]');
        $icon = @$arr[0]['href']."";

        $arr = $xml->xpath('//meta[@property="og:title"]');
        
        $title = @$arr[0]['content']."";
        
        if(!$title)
        {
            $arr = $xml->xpath('//title');
            $title = @$arr[0]."";
        }
        
        $arr = $xml->xpath('//meta[@name="origen"]');
        $origen = @$arr[0]['content']."";
        if(!$origen)
        {
            $arr = $xml->xpath('//meta[@property="og:site_name"]');
            $origen = @$arr[0]['content']."";
        }

        $arr = $xml->xpath('//meta[@name="description"]');
        $description = @$arr[0]['content']."";
        

        $arr = $xml->xpath('//meta[@property="og:image"]');
        $image = trim(@$arr[0]['content']."");
        if(!$image)
        {
            $arr = $xml->xpath('//img');
            $image = trim(@$arr[0]['src']."");
        }
        
        return compact("icon","origen","title","description","image");

    }
}