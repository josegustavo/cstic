<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\RsActivity;
use Input;
use Validator;
use Auth;


class ResourceServiceController extends ApiController
{
	protected function validator($model, array $data)
    {
		$validators = [
            'name' => 'required|string|max:255',
        ];
		
		$data['slug'] = str_slug($data['name']);
		
		if(!$model->slug || ($model->slug != $data['slug']))
			$validators['slug'] = 'unique:resource_services|required|string|max:255';
		//dd($validators);
        return Validator::make($data, $validators);
    }
    
	public function show($id)
    {
		$model = static::$model::select();

        if($with = $this->getWith())
			$model->with($with);
			
		$model->orWhere(['slug' => $id]);
		$model->orWhere(['_id' => $id]);
		
		$result = $model->first();
        return $result;
	}

	protected function getWith()
	{
		return ['image','creator','department','supplier','rs_rates'];
	}
	
    public function index()
    {
		$query = static::$model::with($this->getWith());

		if($collection = Input::get('collection'))
		{
			if($collection == 'my_suggestions')
			{
				$query->where(["created_by" => Auth::user()->_id]);
			}
			else if($collection == 'my_rates')
			{
				$rates = \App\Models\Rate::where(['created_by'=>Auth::user()->_id])->get(['rs_id']);
				$rs_ids = [];
				foreach($rates as $rate)
				{
					$rs_ids []= $rate->rs_id;
				}
				
				$query->whereIn('_id', $rs_ids);
			}
		}
		else
		{
			$activities = array_filter(explode(',', Input::get('activities')));
			if($activities)
			{
				$query->where(["rs_activities.activity_id" => ['$in' => $activities]]);
			}

			$types = array_filter(explode(',', Input::get('types')));
			if($types)
			{
				$query->where(["type_id" => ['$in' => $types]]);
			}

			$suppliers = array_filter(explode(',', Input::get('suppliers')));
			if($suppliers)
			{
				$query->where(["supplier_id" => ['$in' => $suppliers]]);
			}

			$departments = array_filter(explode(',', Input::get('departments')));
			if($departments)
			{
				$query->where(["department_id" => ['$in' => $departments]]);
			}

			$search = Input::get('search');
			if($search)
			{
				//$query->whereRaw(['$text' => ['$search' => $search, '$caseSensitive' => false, '$diacriticSensitive' => false]]);
				//$query->whereRaw(['search' => ['$regex' => $search, '$options' => 'i' ]]);
				//$query->whereRaw(['description' => ['$regex' => $search, '$options' => 'i' ]]);
				$query->whereRaw(['$or' => [
					['tags' => ['$regex' => $search, '$options' => 'i' ]],
					['name' => ['$regex' => $search, '$options' => 'i' ]],
					['description' => ['$regex' => $search, '$options' => 'i' ]],
				]]);
			}	
		}

		$order = Input::get('order','asc');
		$order_by = Input::get('order_by',0);

		$orders = [
			'updated_at',
			'slug',
		];

		$query->orderBy($orders[$order_by], $order);

		return $query->get();        
    }

    private function store_activities($resource_service_id)
    {
    	$rs_activities = Input::post('rs_activities');
    	
    	RsActivity::where('resource_service_id', $resource_service_id)->delete();

        foreach ($rs_activities as $rs_activity) {
        	$activity_id = $rs_activity['activity_id'];
        	$details = $rs_activity['details'];

	    	if($id = $rs_activity['_id']??null)
	    		$rs_activity_model = RsActivity::findOrFail($id);
	    	else
	    		$rs_activity_model = RsActivity::where([
	    			'resource_service_id' => $resource_service_id, 
	    			'activity_id' => $activity_id
	    		])->first();
	    	
	    	if(!$rs_activity_model) $rs_activity_model = new RsActivity;
	        
        	$rs_activity_model->resource_service_id = $resource_service_id;
        	$rs_activity_model->activity_id = $activity_id;
        	$rs_activity_model->details = $details;
        	$rs_activity_model->save();
        }
	}
	

    protected function save(&$model)
    {
		$data = request()->all();
		$this->validator($model, $data)->validate();

		if($model->_id)
		{
			if(Auth::user()->role!='admin' &&  Auth::user()->_id != $model->created_by)
			{
				return;
			}
		}

		if(!$model->supplier_id && Auth::user()->supplier_id)
		{
			$model->supplier_id = Auth::user()->supplier_id;
		}
		if(Auth::user()->role != 'admin')
		{
			$model->supplier_id = Auth::user()->supplier_id;
		}

		if(!$model->department_id && Auth::user()->department_id)
		{
			$model->department_id = Auth::user()->department_id;
		}
		

		$model->slug = str_slug($data['name']);
		
    	$result = parent::save($model);
    	if($result)
    	{
    		//$this->store_activities($model->id);
    	}
    	return $result;
    }

}