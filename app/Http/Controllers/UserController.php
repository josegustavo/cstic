<?php

namespace App\Http\Controllers;

use Input;

use Illuminate\Http\Request;


class UserController extends ApiController
{
    
    public function index()
    {
        $query = static::$model::select();
        
        $search = Input::get('search');
        if($search)
        {
            $query->whereRaw(['$or' => [
                ['username' => ['$regex' => $search, '$options' => 'i' ]],
                ['name' => ['$regex' => $search, '$options' => 'i' ]],
            ]]);
        }

        $query->limit(20);
        $query->orderBy('created_at', 'DESC');
        $result = $query->get();
            
        
        return $result;
    }


    protected function validator(array $data)
    {
        if(!isset($data['username']))
        {
            $username = explode('@',$data['email']??'');
            $data['username'] = $data['username'] ?? $username[0];
        }
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'username' => 'unique:users|required|string|max:32',
            //'email' => 'required|unique:users|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'department_id' => 'required|string',
            'supplier_id' => 'required|string',
        ]);
    }

    protected function create(array $data)
    {
        if(!isset($data['username']))
        {
            $email = $data['email']??'';
            $data['username'] = $data['username'] ?? explode('@',$email);
        }
        if(!isset($data['password']))
        {
            $data['password'] = $data['username'];
        }
        $user = User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email']??'',
            'department_id' => $data['department_id']??'',
            'supplier_id' => $data['supplier_id']??'',
            'password' => bcrypt($data['password']),
        ]);
        return $user;
    }

    protected function save(&$model)
    {
        foreach (Input::all() as $key => $value)
        {
            $model->$key = $value;
        }

        if(!$model->password)
            $model->password = bcrypt($model->username);
        
        if(!$model->supplier_id)
            $model->supplier_id = self::$supplier_id;
        if(!$model->role)
            $model->role = 'instructor';

        $save = $model->save();
        return $save;
    }


    protected static $supplier_id = '59cdb55a32f9eb1a30005d43';//Proveedor Otros
    public function import(Request $request)
    {
        $this->validate($request, [
                'file' => 'required|mimes:csv,application/vnd.ms-excel,txt|max:4096',
        ]);
        $file = $request->file('file');
        $csv_filename = $file->getPathName();
        set_time_limit(0);

        $fopen = fopen($csv_filename, "r");
        if (!$fopen) throw new Exception("No se pudo abrir correctamente el archivo", 1);
	
		$success = 0;        
        $first_row = fgetcsv($fopen, 0, ";");
        while (($row = fgetcsv($fopen, 0, ";")) !== false) {
        	if(!$row[1]) continue;
        	$department_name = str_replace('Departamento de ', '', $row[9]);
        	$user = \App\Models\User::updateOrCreate([
    			'username' => $row[1]
    		],[
        		'name' => implode(' ', [$row[4],$row[2],$row[3]]),
        		'password' => bcrypt($row[1]),
        		'department_id' => $this->getDepartment($department_name),
        		'supplier_id' => self::$supplier_id,
        		'role' => 'instructor',
        	]);
        	if($user)
        		$success ++;
        }
	    return ['success' => $success];
    }

    protected static $departments = null;
    protected function getDepartment(string $name)
    {
    	if(isset(self::$departments[$name]))
    		return self::$departments[$name];

		$department = \App\Models\Department::updateOrCreate(['name' => $name],['supplier_id' => self::$supplier_id]);	
		self::$departments[$name] = $department->_id;		
		return self::$departments[$name];
    }
}