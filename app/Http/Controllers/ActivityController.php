<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ActivityController extends ApiController
{
    public function show($id)
    {
        $model = static::$model::with('image')->findOrFail($id);
        
        return $model;
    }

    public function index()
    {
        return static::$model::with('image')->get();        
    }
}