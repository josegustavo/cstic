<?php

namespace App\Models;

use DB;

class ResourceService extends BaseModel
{
    //protected $fillable = ['score_avg','score_details'];

    public function supplier()
    {
        return $this->belongsTo('App\Models\Supplier');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\Type');
    }

    public function image()
    {
        return $this->belongsTo('App\Models\File');
    }

    public function department()
    {
        return $this->belongsTo('App\Models\Department');
    }

    public function rs_rates()
    {
        return $this->hasOne('App\Models\RsRate', 'rs_id');
    }

    public static function calcRate($rs_id)
    {
        $rates = \App\Models\Rate::where('rs_id',$rs_id)->get(['score']);

        
        $rates_sum = 0;
        $rates_count = count($rates);
        $rates_detail = [0,0,0,0,0];
        foreach($rates as $rate)
        {
            $score = min(5,max(intval($rate['score']),0));
            $rates_sum += $score;
            $rates_detail[$score-1]++;
        }

        $score_avg = round(($rates_count>0)?($rates_sum/$rates_count):0,1);
        
        $rates_styles = ['md-star-outline','md-star-outline','md-star-outline','md-star-outline','md-star-outline'];
        for ($i=0.0; $i < 5.0; $i++) {
        
            if(($score_avg)>=($i+0.5)){
                $rates_styles[$i] = 'md-star-half';
            }
            if(($score_avg-1)>=($i)){
                $rates_styles[$i] = 'md-star';
            }
        }
        
        if($rates_count)
        {
            $data = [
                'styles' => $rates_styles,
                'count' => $rates_count,
                'avg' => $score_avg,
                'details' => $rates_detail
            ];
            DB::collection('rs_rates')->where('rs_id', $rs_id)->update($data, ['upsert' => true]);
        }
        else
        {
            $exist = DB::collection('rs_rates')->where('rs_id', $rs_id)->delete();
            
        }

        
    }
}