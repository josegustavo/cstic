<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Type extends Model
{
    public function image()
    {
        return $this->belongsTo('App\Models\File');
    }

}