<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Department extends Model
{

	protected $fillable = ['name','supplier_id'];

    public function supplier()
    {
        return $this->belongsTo('App\Models\Supplier');
    }
}