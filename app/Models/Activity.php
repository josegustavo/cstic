<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Activity extends Model
{
    public function image()
    {
        return $this->belongsTo('App\Models\File');
    }
}