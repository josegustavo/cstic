<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class RsActivity extends Model
{
    public function activity()
    {
        return $this->belongsTo('App\Models\Activity');
    }

    public function resource_service()
    {
        return $this->belongsTo('App\Models\ResourceService');
    }
}