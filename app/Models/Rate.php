<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Rate extends Model
{
    public function creator()
    {
        return $this->belongsTo('App\User', 'created_by');
    }
}
