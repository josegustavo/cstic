<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class BaseModel extends Model
{
    
    public function creator()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updater()
    {
        dd("ok");
        return $this->belongsTo('App\User', 'updated_by');
    }
}