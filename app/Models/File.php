<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use App\Events\FileRetrieved;

class File extends Model
{
    protected $fillable = ['md5'];

    protected $dispatchesEvents = [
        'retrieved' => FileRetrieved::class,
    ];
}