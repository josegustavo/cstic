<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Supplier extends Model
{
    public function image()
    {
        return $this->belongsTo('App\Models\File');
    }

    
    public function departments()
    {
        return $this->hasMany('App\Models\Department');
    }
}