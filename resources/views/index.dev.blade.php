<!DOCTYPE html>
<html {{$refresh='v38' }} lang="{{ app()->getLocale() }}" ng-app="cstic" ng-class="{'full-page-map': isFullPageMap}">

<head>
    <meta charset="utf-8">
    <base href="/">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="CSTIC">
    <meta name="author" content="Theme Guys - The Netherlands">
    <meta name="msapplication-TileColor" content="#9f00a7">
    <meta name="msapplication-TileImage" content="{{config('app.cdn_url')}}/assets/img/favicon/mstile-144x144.png?{{$refresh}}">
    <meta name="msapplication-config" content="{{config('app.cdn_url')}}/assets/img/favicon/browserconfig.xml?{{$refresh}}">
    <meta name="theme-color" content="#ffffff">
    <link rel="apple-touch-icon" sizes="57x57" href="{{config('app.cdn_url')}}/assets/img/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{{config('app.cdn_url')}}/assets/img/favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{config('app.cdn_url')}}/assets/img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{{config('app.cdn_url')}}/assets/img/favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{config('app.cdn_url')}}/assets/img/favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{config('app.cdn_url')}}/assets/img/favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{config('app.cdn_url')}}/assets/img/favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{config('app.cdn_url')}}/assets/img/favicon/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{config('app.cdn_url')}}/assets/img/favicon/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="{{config('app.cdn_url')}}/assets/img/favicon/favicon-32x32.png?{{$refresh}}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{config('app.cdn_url')}}/assets/img/favicon/android-chrome-192x192.png?{{$refresh}}" sizes="192x192">
    <link rel="icon" type="image/png" href="{{config('app.cdn_url')}}/assets/img/favicon/favicon-96x96.png?{{$refresh}}" sizes="96x96">
    <link rel="icon" type="image/png" href="{{config('app.cdn_url')}}/assets/img/favicon/favicon-16x16.png?{{$refresh}}" sizes="16x16">
    <link rel="manifest" href="{{config('app.cdn_url')}}/assets/img/favicon/manifest.json?{{$refresh}}">
    <link rel="shortcut icon" href="{{config('app.cdn_url')}}/assets/img/favicon/favicon.ico?{{$refresh}}">
    <title ng-bind="pageTitle + ''">Catologo de Recuros y Servicios Tecnológicos</title>
    <!-- build:css  /assets/css/vendors.min.css -->
    <link href="bower/material-design-iconic-font/css/material-design-iconic-font.css" rel="stylesheet" />
    <link href="bower/angular-loading-bar/build/loading-bar.css" rel="stylesheet" />
    <link href="bower/angular-motion/dist/angular-motion.css" rel="stylesheet" />
    <link href="bower/bootstrap-additions/dist/bootstrap-additions.css" rel="stylesheet" />
    <link href='bower/textAngular/src/textAngular.css' rel="stylesheet" />
    <link href="bower/angular-ui-select/dist/select.css" rel="stylesheet" />
    <link href="bower/ng-table/dist/ng-table.css" rel="stylesheet" />
    <!-- endbuild -->
    <!-- build:css /assets/css/styles.min.css -->
    <link href="css/materialism.css" rel="stylesheet" />
    <link href="css/helpers.css" rel="stylesheet" />
    <link href="css/ripples.css" rel="stylesheet" />
    <!-- endbuild -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T975Q6C');</script>
<!-- End Google Tag Manager -->
</head>

<body ng-controller="MainController" scroll-spy id="top" ng-class="[theme.template, theme.color]">
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T975Q6C"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <main>
        <div class="main-container">
            <div ng-include src="'{{config('app.cdn_url')}}/assets/tpl/partials/topnav.html'"></div>
            @verbatim
            <div class="main-content" style="padding-top:{{(scroll && scroll>20)?'64px':'0px'}}"><ui-view></ui-view></div>
            @endverbatim
        </div>
    </main>
    <!--[if lt IE 10 ]>
  <script>
    // this is why we can't have nice things
    var browser_old = true;
  </script>
  <![endif]-->
    <!-- build:js  /assets/js/vendors.min.js -->
    <script charset="utf-8" src="bower/jquery/dist/jquery.min.js"></script>
    <script charset="utf-8" src="bower/bootstrap-sass/assets/javascripts/bootstrap.js"></script>
    <script charset="utf-8" src="bower/angular/angular.js"></script>
    <script charset="utf-8" src="bower/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script charset="utf-8" src="bower/angular-ui-select/dist/select.js"></script>
    <script charset="utf-8" src="bower/angular-sanitize/angular-sanitize.js"></script>
    <script charset="utf-8" src="bower/angular-local-storage/dist/angular-local-storage.js"></script>
    <script charset="utf-8" src="bower/angular-loading-bar/build/loading-bar.js"></script>
    <script charset="utf-8" src="bower/angular-strap/dist/angular-strap.js"></script>
    <script charset="utf-8" src="bower/angular-strap/dist/angular-strap.tpl.js"></script>
    <!-- shim is needed to support non-HTML5 FormData browsers (IE8-9) -->
    <script charset="utf-8" src="js/vendors/ng-file-upload-file-api.js"></script>
    <script charset="utf-8" src="bower/hammerjs/hammer.js"></script>
    <script charset="utf-8" src="bower/velocity/velocity.js"></script>
    <script charset="utf-8" src="bower/select2/select2.js"></script>
    <script charset="utf-8" src="bower/nouislider/distribute/jquery.nouislider.js"></script>
    <script charset="utf-8" src="bower/ng-file-upload/ng-file-upload-shim.js"></script>
    <script charset="utf-8" src="bower/ng-file-upload/ng-file-upload.js"></script>
    <script charset="utf-8" src="bower/textAngular/dist/textAngular-rangy.min.js"></script>
    <script charset="utf-8" src="bower/textAngular/dist/textAngular-sanitize.min.js"></script>
    <script charset="utf-8" src="bower/textAngular/dist/textAngular.min.js"></script>
    <script charset="utf-8" src="bower/angular-timeago/dist/angular-timeago.js"></script>
    <script charset="utf-8" src="bower/Sortable/Sortable.js"></script>
    <script charset="utf-8" src="bower/Sortable/ng-sortable.js"></script>
    <!-- endbuild -->

    <script type="text/javascript">
    angular.module('app.constants', []).constant('APP', {
        version: '1.0.0',
        refresh: '{{$refresh}}',
        cdn_url: '{{config("app.cdn_url")}}',
        @if(Auth::check())
        user: @php echo json_encode(Auth::user()) @endphp,
        @endif
    });
    </script>


    <!-- build:js /assets/js/app.min.js -->
    <script charset="utf-8" src="js/vendors/side-nav.js"></script>
    <script charset="utf-8" src="js/vendors/ripples.js"></script>
    <script charset="utf-8" src="js/vendors/init-ripples.js"></script>
    <script charset="utf-8" src="js/vendors/fsm-sticky-header.js"></script>
    <script charset="utf-8" src="js/vendors/angular-smooth-scroll.js"></script>
    <script charset="utf-8" src="js/app.js"></script>
    <script charset="utf-8" src="js/app.config.js"></script>
    <script charset="utf-8" src="js/app.filters.js"></script>
    <script charset="utf-8" src="js/directives/formcontrol.js"></script>
    <script charset="utf-8" src="js/directives/navbar-hover.js"></script>
    <script charset="utf-8" src="js/directives/navbar-search.js"></script>
    <script charset="utf-8" src="js/directives/noui-slider.js"></script>
    <script charset="utf-8" src="js/directives/menu-link.js"></script>
    <script charset="utf-8" src="js/directives/menu-toggle.js"></script>
    <script charset="utf-8" src="js/directives/scroll-spy.js"></script>
    <script charset="utf-8" src="js/controllers/main.js"></script>
    <script charset="utf-8" src="js/controllers/home.js"></script>
    <script charset="utf-8" src="js/controllers/search.js"></script>
    <script charset="utf-8" src="js/controllers/rs.js"></script>
    <script charset="utf-8" src="js/controllers/rs-create.js"></script>
    <script charset="utf-8" src="js/controllers/rs-edit.js"></script>
    <script charset="utf-8" src="js/controllers/apps/crud-type.js"></script>
    <script charset="utf-8" src="js/controllers/apps/crud-activity.js"></script>
    <script charset="utf-8" src="js/controllers/apps/crud-supplier.js"></script>
    <script charset="utf-8" src="js/controllers/apps/crud-department.js"></script>
    <script charset="utf-8" src="js/controllers/apps/crud-resource-service.js"></script>
    <script charset="utf-8" src="js/controllers/apps/crud-feature.js"></script>
    <script charset="utf-8" src="js/controllers/apps/crud-keyword.js"></script>
    <script charset="utf-8" src="js/controllers/apps/crud-user.js"></script>
    <script charset="utf-8" src="js/controllers/login.js"></script>
    <!-- endbuild -->
    
</body>

</html>