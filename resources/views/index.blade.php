<!DOCTYPE html>
<html {{$refresh='v38' }} lang="{{ app()->getLocale() }}" ng-app="cstic" ng-class="{'full-page-map': isFullPageMap}">

<head>
    <meta charset="utf-8">
    <base href="/">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="CSTIC">
    <meta name="author" content="Theme Guys - The Netherlands">
    <meta name="msapplication-TileColor" content="#9f00a7">
    <meta name="msapplication-TileImage" content="{{config('app.cdn_url')}}/assets/img/favicon/mstile-144x144.png?{{$refresh}}">
    <meta name="msapplication-config" content="{{config('app.cdn_url')}}/assets/img/favicon/browserconfig.xml?{{$refresh}}">
    <meta name="theme-color" content="#ffffff">
    <link rel="apple-touch-icon" sizes="57x57" href="{{config('app.cdn_url')}}/assets/img/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{{config('app.cdn_url')}}/assets/img/favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{config('app.cdn_url')}}/assets/img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{{config('app.cdn_url')}}/assets/img/favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{config('app.cdn_url')}}/assets/img/favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{config('app.cdn_url')}}/assets/img/favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{config('app.cdn_url')}}/assets/img/favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{config('app.cdn_url')}}/assets/img/favicon/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{config('app.cdn_url')}}/assets/img/favicon/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="{{config('app.cdn_url')}}/assets/img/favicon/favicon-32x32.png?{{$refresh}}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{config('app.cdn_url')}}/assets/img/favicon/android-chrome-192x192.png?{{$refresh}}" sizes="192x192">
    <link rel="icon" type="image/png" href="{{config('app.cdn_url')}}/assets/img/favicon/favicon-96x96.png?{{$refresh}}" sizes="96x96">
    <link rel="icon" type="image/png" href="{{config('app.cdn_url')}}/assets/img/favicon/favicon-16x16.png?{{$refresh}}" sizes="16x16">
    <link rel="manifest" href="{{config('app.cdn_url')}}/assets/img/favicon/manifest.json?{{$refresh}}">
    <link rel="shortcut icon" href="{{config('app.cdn_url')}}/assets/img/favicon/favicon.ico?{{$refresh}}">
    <title ng-bind="pageTitle + ''">Catologo de Recuros y Servicios Tecnológicos</title>
    <link rel="stylesheet" href="{{config('app.cdn_url')}}/assets/css/vendors.min.bbe2e48ae66e0c52ac38.css">
    <link rel="stylesheet" href="{{config('app.cdn_url')}}/assets/css/styles.min.4e4e6c7f7a5d933e3bb9.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T975Q6C');</script>
<!-- End Google Tag Manager -->
</head>

<body ng-controller="MainController" scroll-spy id="top" ng-class="[theme.template, theme.color]">
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T975Q6C"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <main>
        <div class="main-container">
            <div ng-include src="'{{config('app.cdn_url')}}/assets/tpl/partials/topnav.html'"></div>
            @verbatim
            <div class="main-content" style="padding-top:{{(scroll && scroll>20)?'64px':'0px'}}"><ui-view></ui-view></div>
            @endverbatim
        </div>
    </main>
    <!--[if lt IE 10 ]>
  <script>
    // this is why we can't have nice things
    var browser_old = true;
  </script>
  <![endif]-->
    <script src="{{config('app.cdn_url')}}/assets/js/vendors.min.fa282b4468780c1e7c81.js"></script>

    <script type="text/javascript">
    angular.module('app.constants', []).constant('APP', {
        version: '1.0.0',
        refresh: '{{$refresh}}',
        cdn_url: '{{config("app.cdn_url")}}',
        @if(Auth::check())
        user: @php echo json_encode(Auth::user()) @endphp,
        @endif
    });
    </script>


    <script src="{{config('app.cdn_url')}}/assets/js/app.min.1849c10c767cc524aec1.js"></script>
    
</body>

</html>