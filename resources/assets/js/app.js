/*jslint strict: true */

var app = angular.module('cstic', [
    'app.constants',

    //'ngRoute',
    'ui.router',
    //'ngAnimate',
    'ngSanitize',
    //'ngPlaceholders',
    //'ngTable',

    'angular-loading-bar',

    //'uiGmapgoogle-maps',
    'ui.select',

    //'gridshore.c3js.chart',
    //'monospaced.elastic', // resizable textarea
    'mgcrea.ngStrap',
    //'jcs-autoValidate',
    'ngFileUpload',
    'textAngular',
    //'fsm',                    // sticky header
    //'smoothScroll',
    'LocalStorageModule',

    'yaru22.angular-timeago',
    'ng-sortable',
]);