// routes
app.config(['$stateProvider', '$locationProvider', 'timeAgoSettings', 'APP', '$provide',
        function($stateProvider, $locationProvider, timeAgoSettings, APP, $provide) {
            $stateProvider.state('index', {
                url: '/',
                templateUrl: APP.cdn_url + '/assets/tpl/home.html?' + APP.refresh
            }).state('search_blank', {
                url: '/buscar',
                templateUrl: APP.cdn_url + '/assets/tpl/search.html?' + APP.refresh
            }).state('search', {
                url: '/buscar/:texto',
                templateUrl: APP.cdn_url + '/assets/tpl/search.html?' + APP.refresh
            }).state('my-suggestions', {
                url: '/mis-sugerencias',
                params: { collection: 'my_suggestions' },
                templateUrl: APP.cdn_url + '/assets/tpl/search.html?' + APP.refresh
            }).state('my-rates', {
                url: '/mis-calificaciones',
                params: { collection: 'my_rates' },
                templateUrl: APP.cdn_url + '/assets/tpl/search.html?' + APP.refresh
            }).state('rs', {
                url: '/rs/:slug',
                templateUrl: APP.cdn_url + '/assets/tpl/rs.html?' + APP.refresh
            }).state('rs-new', {
                url: '/rs/nuevo',
                controller: 'RsEditController',
                templateUrl: APP.cdn_url + '/assets/tpl/rs-edit.html?' + APP.refresh
            }).state('rs-edit', {
                url: '/rs/:slug/editar',
                controller: 'RsEditController',
                templateUrl: APP.cdn_url + '/assets/tpl/rs-edit.html?' + APP.refresh
            }).state('rs-action', {
                url: '/rs/:slug/:action',
                templateUrl: APP.cdn_url + '/assets/tpl/rs.html?' + APP.refresh
            }).state('folder-tpl', {
                url: '/:folder/:tpl',
                templateUrl: function(attr) {
                    return APP.cdn_url + '/assets/tpl/' + attr.folder + '/' + attr.tpl + '.html?' + APP.refresh;
                }
            }).state('tpl', {
                url: '/:tpl',
                templateUrl: function(attr) {
                    return APP.cdn_url + '/assets/tpl/' + attr.tpl + '.html?' + APP.refresh;
                }
            }); //.otherwise({ redirectTo: '/' });

            // use the HTML5 History API
            $locationProvider.html5Mode(true);

            timeAgoSettings.overrideLang = 'es_LA';

        }
    ])
    .config(['$sceDelegateProvider', '$sceProvider', 'APP', function($sceDelegateProvider, $sceProvider, APP) {
        $sceDelegateProvider.resourceUrlWhitelist([
            // Allow same origin resource loads.
            'self',
            // Allow loading from our assets domain.  Notice the difference between * and **.
            location.protocol+APP.cdn_url + '/**'
        ]);
        //$sceProvider.enabled(false);
    }])
    .config(['$provide', function($provide) {
        $provide.decorator('taOptions', ['taCustomRenderers', '$delegate', function(taCustomRenderers, taOptions) {
            taCustomRenderers.push({
                selector: 'a',
                renderLogic: function(element) {
                    element.attr('target', '_blank');
                }
            });
            return taOptions;
        }]);
    }])

/*
// google maps
.config(['uiGmapGoogleMapApiProvider', function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        //    key: 'your api key',
        v: '3.17',
        libraries: 'weather,geometry,visualization'
    });
}])
*/
// loading bar settings
.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
    cfpLoadingBarProvider.latencyThreshold = 1;
}])

/*
// defaults for date picker
.config(['$datepickerProvider', function($datepickerProvider) {
    angular.extend($datepickerProvider.defaults, {
        dateFormat: 'dd/MM/yyyy',
        iconLeft: 'md md-chevron-left',
        iconRight: 'md md-chevron-right',
        autoclose: true,
    });
}])
*/

/*
// defaults for date picker
.config(['$timepickerProvider', function($timepickerProvider) {
    angular.extend($timepickerProvider.defaults, {
        timeFormat: 'HH:mm',
        iconUp: 'md md-expand-less',
        iconDown: 'md md-expand-more',
        hourStep: 1,
        minuteStep: 1,
        arrowBehavior: 'picker'
    });
}])
*/

/*
// disable nganimate with adding class
.config(['$animateProvider', function($animateProvider) {
    $animateProvider.classNameFilter(/^(?:(?!ng-animate-disabled).)*$/);
}])
*/

// set constants
.run(['$rootScope', 'APP', function($rootScope, APP) {
    $rootScope.APP = APP;
}]);