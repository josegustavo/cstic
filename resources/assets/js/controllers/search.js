app.controller('SearchController', ['$window', '$scope', '$rootScope', '$aside', '$interval', '$http', '$location', '$state', 'APP',

    function($window, $scope, $rootScope, $aside, $interval, $http, $location, $state, APP) {
        console.log('run SearchController');

        $rootScope.pageTitle = 'Resultados de búsqueda';
        $rootScope.hideLogo = false;

        $scope.order_options = [
            'Por fecha',
            'Por nombre',
        ];

        $scope.collection = $state.params.collection;

        $scope.changeOrder = function(index) {
            if (index == $scope.search_config.order_by_index) {
                $scope.search_config.order_asc = !$scope.search_config.order_asc;
            } else {
                $scope.search_config.order_by_index = index;
            }
            goToSearch();
        };

        $rootScope.formSearchTpl = $aside({
            scope: $scope,
            templateUrl: '/assets/tpl/search-aside.html?' + APP.refresh,
            show: false,
            placement: 'left',
            backdrop: false,
            animation: 'am-slide-left'
        });

        $scope.showForm = function() {
            angular.element('.tooltip').remove();
            $rootScope.formSearchTpl.show();
        };

        $scope.hideForm = function() {
            $rootScope.formSearchTpl.hide();
        };

        $scope.toogleForm = function() {
            $rootScope.formSearchTpl.toggle();
        };
        $scope.$on('$destroy', function() {
            console.log('run SearchController $destroy');
            $scope.hideForm();
        });

        $scope.local_config.search_text = $state.params.texto;

        $scope.clearAdvanceSearch = function() {
            $scope.resetSearchConfig();
            goToSearch();
        };

        var parent_goToSearch = $scope.goToSearch;
        var goToSearch = $scope.goToSearch = function() {
            $scope.hideForm();
            if ($state.params.texto != $scope.local_config.search_text)
                parent_goToSearch();
            else
                $scope.getSearch();
        };

        $scope.getSearch = function() {
            var params = {};
            if ($scope.collection)
                params.collection = $scope.collection;
            else
                params.search = $scope.local_config.search_text;

            if (!$scope.collection && $scope.search_config.block_advance_search.active) {
                var types_ids = [];
                angular.forEach($scope.search_config.block_types.types, function(t, id) {
                    if (t.checked) types_ids.push(id);
                });
                if (types_ids.length && (types_ids.length != $scope.types.length))
                    params.types = types_ids.join(',');

                var activities_ids = [];
                angular.forEach($scope.search_config.block_activities.activities, function(t, id) {
                    if (t.checked) activities_ids.push(id);
                });
                if (activities_ids.length && (activities_ids.length != $scope.activities.length))
                    params.activities = activities_ids.join(',');

                var suppliers_ids = [];
                var departments_ids = [];
                var count_departments = 0;
                angular.forEach($scope.search_config.block_suppliers.suppliers, function(t, id) {
                    if (t.checked) suppliers_ids.push(id);

                    if (t.departments_ids) {
                        angular.forEach(t.departments_ids, function(d) {
                            departments_ids.push(d);
                        });
                        count_departments += t.departments_ids.length;
                    }
                });
                if (suppliers_ids.length)
                    params.suppliers = suppliers_ids.join(',');

                if (departments_ids.length)
                    params.departments = departments_ids.join(',');
            }

            params.order = $scope.search_config.order_asc ? 'asc' : 'desc';
            params.order_by = $scope.search_config.order_by_index;

            var config_get = { params: params };
            $scope.searching = true;
            $http.get('/api/resource_service', config_get).then(function(res) {
                $scope.results = res.data;
            }).finally(function() {
                $scope.searching = false;
            });
        };

        $scope.getSearch();

        $scope.removeRS = function(item) {
            if (confirm("¿Realmente desea eliminar este elemento?\nNo podrá recuperarlo después.")) {
                $http.delete('/api/resource_service/' + item._id).then(function() {
                    $scope.results.splice($scope.results.indexOf(item), 1);
                });

            }
        };
    }
]);