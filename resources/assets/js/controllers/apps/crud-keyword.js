app.controller('CrudKeywordController', ['$scope', '$window', '$aside', '$http',
    function($scope, $window, $aside, $http) {

        // settings
        $scope.settings = {
            singular: 'Palabra clave',
            plural: 'Palabras claves',
            cmd: 'Add'
        };

        var name = 'keyword';
        var endpoint = '/api/' + name;
        // adding demo data


        //Cargar datos
        $http.get(endpoint).then(function(res) {
            $scope.data = res.data;
        });

        // defining template
        var formTpl = $aside({
            scope: $scope,
            templateUrl: 'assets/tpl/apps/crud-' + name + '-form.html',
            show: false,
            placement: 'left',
            backdrop: false,
            animation: 'am-slide-left'
        });

        // methods
        $scope.checkAll = function() {
            angular.forEach($scope.data, function(item) {
                item.selected = !item.selected;
            });
        };

        $scope.editItem = function(item) {
            if (item) {
                item.editing = true;
                $scope.item = item;
                $scope.settings.cmd = 'Edit';
                showForm();
            }
        };

        $scope.viewItem = function(item) {
            if (item) {
                item.editing = false;
                $scope.item = item;
                $scope.settings.cmd = 'View';
                showForm();
            }
        };

        $scope.createItem = function() {
            var item = {

                editing: true
            };
            $scope.item = item;
            $scope.settings.cmd = 'New';
            showForm();
        };

        $scope.saveItem = function() {

            if ($scope.settings.cmd == 'New') {

                $http.post(endpoint, $scope.item).then(function($result) {
                    $scope.item._id = $result.data.success;
                    $scope.data.push($scope.item);
                });
            } else {
                $http.put(endpoint + '/' + $scope.item._id, $scope.item);
            }
            hideForm();
        };

        $scope.remove = function(item) {
            if (confirm('Are you sure?')) {
                if (item) {
                    $http.delete(endpoint + '/' + item._id).then(function() {
                        $scope.data.splice($scope.data.indexOf(item), 1);
                    });
                } else {
                    var new_data = $scope.data.filter(
                        function(item) {
                            return !item.selected;
                        }
                    );
                    var selected = $scope.data.filter(
                        function(item) {
                            return item.selected;
                        }
                    );
                    var ids = selected.map(function(item) {
                        return item._id;
                    });
                    $http.delete(endpoint, { data: ids, headers: { 'Content-Type': 'application/json' } }).then(function() {
                        $scope.data = new_data;
                    });


                    $scope.selectAll = false;
                }
            }
        };

        showForm = function() {
            angular.element('.tooltip').remove();
            formTpl.show();
        };

        hideForm = function() {
            formTpl.hide();
        };

        $scope.$on('$destroy', function() {
            hideForm();
        });

    }
]);