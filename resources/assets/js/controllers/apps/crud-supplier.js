app.controller('CrudSupplierController', ['$scope', '$window', '$aside', '$http',
    function($scope, $window, $aside, $http) {

        // settings
        $scope.settings = {
            singular: 'Proveedor',
            plural: 'Proveedores',
            cmd: 'Add'
        };

        var name = 'supplier';
        var endpoint = '/api/' + name;
        // adding demo data

        var upload = $scope.uploadImage;
        $scope.uploadImage = function() {
            upload(function(data) {
                $scope.item.image = data;
            });
        };

        //Cargar datos
        $http.get(endpoint).then(function(res) {
            $scope.data = res.data;
        });

        // defining template
        var formTpl = $aside({
            scope: $scope,
            template: 'assets/tpl/apps/crud-' + name + '-form.html',
            show: false,
            placement: 'left',
            backdrop: false,
            animation: 'am-slide-left'
        });

        // methods
        $scope.checkAll = function() {
            angular.forEach($scope.data, function(item) {
                item.selected = !item.selected;
            });
        };

        $scope.editItem = function(item) {
            if (item) {
                item.editing = true;
                $scope.item = item;
                $scope.settings.cmd = 'Edit';
                showForm();
            }
        };

        $scope.viewItem = function(item) {
            if (item) {
                item.editing = false;
                $scope.item = item;
                $scope.settings.cmd = 'View';
                showForm();
            }
        };

        $scope.createItem = function() {
            var item = {

                editing: true
            };
            $scope.item = item;
            $scope.settings.cmd = 'New';
            showForm();
        };

        $scope.saveItem = function() {

            //Actualizar el id de la imagen
            $scope.item.image_id = $scope.item.image ? $scope.item.image._id : null;

            //Preparar la data a enviar
            var data = {
                image_id: $scope.item.image_id,
                name: $scope.item.name,
                description: $scope.item.description,
            };

            if ($scope.settings.cmd == 'New') {

                $http.post(endpoint, data).then(function($result) {
                    $scope.item._id = $result.data.success;

                    $scope.data.push($scope.item);
                });
            } else {
                $http.put(endpoint + '/' + $scope.item._id, data);
            }

            hideForm();
        };

        $scope.remove = function(item) {
            if (confirm('Are you sure?')) {
                if (item) {
                    $http.delete(endpoint + '/' + item._id).then(function() {
                        $scope.data.splice($scope.data.indexOf(item), 1);
                    });
                } else {
                    var new_data = $scope.data.filter(
                        function(item) {
                            return !item.selected;
                        }
                    );
                    var selected = $scope.data.filter(
                        function(item) {
                            return item.selected;
                        }
                    );
                    var ids = selected.map(function(item) {
                        return item._id;
                    });
                    $http.delete(endpoint, { data: ids, headers: { 'Content-Type': 'application/json' } }).then(function() {
                        $scope.data = new_data;
                    });


                    $scope.selectAll = false;
                }
            }
        };

        showForm = function() {
            angular.element('.tooltip').remove();
            formTpl.show();
        };

        hideForm = function() {
            formTpl.hide();
        };

        $scope.$on('$destroy', function() {
            hideForm();
        });

    }
]);