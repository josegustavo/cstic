app.controller('CrudResourceServiceController', ['$scope', '$window', '$aside', '$http',
    function($scope, $window, $aside, $http) {

        // settings
        $scope.settings = {
            singular: 'Recurso o Servicio',
            plural: 'Recursos o Servicios',
            cmd: 'Add'
        };

        var name = 'resource_service';
        var endpoint = '/api/' + name;
        // adding demo data


        //Cargar datos
        $http.get(endpoint).then(function(res) {
            $scope.data = res.data;
        });

        $http.get('/api/supplier?associative').then(function(res) {
            $scope.suppliers = res.data;
        });

        $http.get('/api/type?associative').then(function(res) {
            $scope.types = res.data;
        });

        $http.get('/api/feature').then(function(res) {
            $scope.features = res.data;
        });

        $http.get('/api/activity').then(function(res) {
            $scope.activities = res.data;
        });

        var upload = $scope.uploadImage;
        $scope.uploadImage = function() {
            upload(function(data) {
                $scope.item.image = data;
            });
        };

        // defining template
        var formTpl = $aside({
            scope: $scope,
            template: 'assets/tpl/apps/crud-' + name.replace('_', '-') + '-form.html',
            show: false,
            placement: 'left',
            backdrop: false,
            animation: 'am-slide-left'
        });

        $scope.addActivity = function() {

            if (!angular.isArray($scope.item.rs_activities))
                $scope.item.rs_activities = [];
            $scope.item.rs_activities.push({ details: [""] });
        };

        $scope.addFeature = function() {

            if (!angular.isArray($scope.item.rs_features))
                $scope.item.rs_features = [];
            $scope.item.rs_features.push({ content: "" });
        };

        // methods
        $scope.checkAll = function() {
            angular.forEach($scope.data, function(item) {
                item.selected = !item.selected;
            });
        };

        $scope.editItem = function(item) {
            if (item) {
                item.editing = true;
                $scope.item = item;
                $scope.settings.cmd = 'Edit';
                showForm();
            }
        };

        $scope.viewItem = function(item) {
            if (item) {
                item.editing = false;
                $scope.item = item;
                $scope.settings.cmd = 'View';
                showForm();
            }
        };

        $scope.createItem = function() {
            var item = {
                editing: true
            };
            $scope.item = item;
            $scope.settings.cmd = 'New';
            showForm();
        };

        $scope.saveItem = function() {
            //Actualizar el id del proveedor
            //$scope.item.supplier_id = $scope.item.supplier ? $scope.item.supplier._id : null;
            //Actualizar el id del tipo
            //$scope.item.type_id = $scope.item.type ? $scope.item.type._id : null;

            var activities_obj = {};
            //Actividades
            angular.forEach($scope.item.rs_activities, function(a) {
                if (a.details && a.details.length) {
                    if (!activities_obj[a.activity_id]) {
                        activities_obj[a.activity_id] = {
                            activity_id: a.activity_id,
                            details: a.details
                        };
                    } else {
                        activities_obj[a.activity_id].details.push(a.details[0]);
                    }
                }
            });
            var activities = [];
            angular.forEach(activities_obj, function(a) {
                activities.push(a);
            });

            var features = [];
            angular.forEach($scope.item.rs_features, function(f) {
                features.push({
                    feature: {
                        name: f.feature.name,
                        description: f.feature.description,
                        type: f.feature.type,
                    },
                    content: f.content,
                });
            });
            /*var search = angular.copy($scope.item.tags);
            search.push($scope.item.name);
            search.push($scope.item.description);*/

            //Preparar la data a enviar
            var data = {
                name: $scope.item.name,
                description: $scope.item.description,
                supplier_id: $scope.item.supplier_id,
                tags: $scope.item.tags,
                department_id: $scope.item.department_id,
                type_id: $scope.item.type_id,
                type: { name: $scope.types[$scope.item.type_id].name },
                rs_activities: activities,
                rs_features: features,
                image_id: $scope.item.image ? $scope.item.image._id : null,
            };

            if ($scope.settings.cmd == 'New') {

                $http.post(endpoint, data).then(function($result) {
                    $scope.item._id = $result.data.success;

                    $scope.data.push($scope.item);
                });
            } else {
                $http.put(endpoint + '/' + $scope.item._id, data);
            }

            hideForm();
        };

        $scope.remove = function(item) {
            if (confirm('Are you sure?')) {
                if (item) {
                    $http.delete(endpoint + '/' + item._id).then(function() {
                        $scope.data.splice($scope.data.indexOf(item), 1);
                    });
                } else {
                    var new_data = $scope.data.filter(
                        function(item) {
                            return !item.selected;
                        }
                    );
                    var selected = $scope.data.filter(
                        function(item) {
                            return item.selected;
                        }
                    );
                    var ids = selected.map(function(item) {
                        return item._id;
                    });
                    $http.delete(endpoint, { data: ids, headers: { 'Content-Type': 'application/json' } }).then(function() {
                        $scope.data = new_data;
                    });


                    $scope.selectAll = false;
                }
            }
        };

        showForm = function() {
            angular.element('.tooltip').remove();
            formTpl.show();
        };

        hideForm = function() {
            formTpl.hide();
        };

        $scope.$on('$destroy', function() {
            hideForm();
        });

    }
]);