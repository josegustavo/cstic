app.controller('CrudUserController', ['$scope', '$window', '$aside', '$http', 'Upload',
    function($scope, $window, $aside, $http, Upload) {

        // settings
        $scope.settings = {
            singular: 'Usuario',
            plural: 'Usuarios',
            cmd: 'Add',
            searchText: ''
        };

        var name = 'user';
        var endpoint = '/api/' + name;

        $scope.importUsers = function(successCallback, errorCallback, progressCallback) {
            var file = document.createElement("input");
            file.setAttribute('type', 'file');
            file.setAttribute('accept', '.csv');

            file.onchange = function(e) {
                if (!file || !file.files || !file.files[0]) return;
                console.log(e);
                return Upload.upload({
                    url: endpoint + '/import',
                    file: file.files[0],

                }).then(
                    function(result) {
                        var data = result.data;
                        if (successCallback) successCallback(data);
                    }, errorCallback, progressCallback);
            };
            file.click();
        };

        $scope.buscar = function() {
            $http.get(endpoint + '?search=' + $scope.settings.searchText).then(function(res) {
                $scope.data = res.data;
            });
        }

        //Cargar datos
        $http.get(endpoint).then(function(res) {
            $scope.data = res.data;
        });

        // defining template
        var formTpl = $aside({
            scope: $scope,
            template: 'assets/tpl/apps/crud-' + name + '-form.html',
            show: false,
            placement: 'left',
            backdrop: false,
            animation: 'am-slide-left'
        });

        // methods
        $scope.checkAll = function() {
            angular.forEach($scope.data, function(item) {
                item.selected = !item.selected;
            });
        };

        $scope.editItem = function(item) {
            if (item) {
                item.editing = true;
                $scope.item = item;
                $scope.settings.cmd = 'Edit';
                showForm();
            }
        };

        $scope.viewItem = function(item) {
            if (item) {
                item.editing = false;
                $scope.item = item;
                $scope.settings.cmd = 'View';
                showForm();
            }
        };

        $scope.createItem = function() {
            var item = {

                editing: true
            };
            $scope.item = item;
            $scope.settings.cmd = 'New';
            showForm();
        };

        $scope.saveItem = function() {

            //Preparar la data a enviar
            var data = {
                username: $scope.item.username,
                name: $scope.item.name,
                role: $scope.item.role,
                supplier_id: $scope.item.supplier_id,
                department_id: $scope.item.department_id,
            };

            if ($scope.settings.cmd == 'New') {

                $http.post(endpoint, data).then(function($result) {
                    $scope.item._id = $result.data.success;

                    $scope.data.push($scope.item);
                });
            } else {
                $http.put(endpoint + '/' + $scope.item._id, data);
            }

            hideForm();
        };

        $scope.remove = function(item) {
            if (confirm('Are you sure?')) {
                if (item) {
                    $http.delete(endpoint + '/' + item._id).then(function() {
                        $scope.data.splice($scope.data.indexOf(item), 1);
                    });
                } else {
                    var new_data = $scope.data.filter(
                        function(item) {
                            return !item.selected;
                        }
                    );
                    var selected = $scope.data.filter(
                        function(item) {
                            return item.selected;
                        }
                    );
                    var ids = selected.map(function(item) {
                        return item._id;
                    });
                    $http.delete(endpoint, { data: ids, headers: { 'Content-Type': 'application/json' } }).then(function() {
                        $scope.data = new_data;
                    });


                    $scope.selectAll = false;
                }
            }
        };

        showForm = function() {
            angular.element('.tooltip').remove();
            formTpl.show();
        };

        hideForm = function() {
            formTpl.hide();
        };

        $scope.$on('$destroy', function() {
            hideForm();
        });
    }
]);