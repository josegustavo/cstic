app.controller('RsController', [
    '$window', '$scope', '$rootScope', '$aside', '$interval', '$http', '$location', '$state', 'APP', '$q', 'localStorageService',
    function($window, $scope, $rootScope, $aside, $interval, $http, $location, $state, APP, $q, localStorageService) {

        console.log('run RsController', $state.params);

        $rootScope.pageTitle = 'Recurso o Serivicio';
        $rootScope.hideLogo = false;

        var rateForm = {
            score: 0,
            content: '',
            activities: {},
            errorSubmitExperience: '',
        };
        var slug = $scope.slug = $state.params.slug;

        var model_load = $q(function(resolve, reject) {
            var rs = $scope.rs = localStorageService.get('rs_' + slug);
            if (rs) resolve($scope.rs);
            $http.get('/api/resource_service/' + slug).then(function(res) {
                $scope.rs = res.data;
                $scope.rs.type = $scope.getType($scope.rs.type_id);
                $scope.rs.supplier = $scope.getSupplier($scope.rs.supplier_id);
                localStorageService.set('rs_' + slug, $scope.rs);
                if (!rs) resolve($scope.rs);
            });

        });

        var loadRate = function() {
            var rateForm_saved = localStorageService.get('rate_' + $state.params.slug);
            if (rateForm_saved) $scope.rateForm = rateForm_saved;
            var params = {
                rs_id: $scope.rs._id,
            };
            $http.get('/api/rate/' + $scope.rs._id).then(function(res) {
                $scope.rateForm = res.data ? res.data : rateForm;
                var activities_ids = {};
                angular.forEach(res.data.activities_ids, function(a) {
                    activities_ids[a] = true;
                });
                $scope.rateForm.activities = activities_ids;
                localStorageService.set('rate_' + $state.params.slug, $scope.rateForm);
            });

        };

        var loadRatings = function() {
            $scope.ratings = localStorageService.get('ratings_' + $state.params.slug);
            var params = {
                params: { rs_id: $scope.rs._id }
            };
            $http.get('/api/rate', params).then(function(result) {
                $scope.ratings = result.data;
                localStorageService.set('ratings_' + $state.params.slug, $scope.ratings);
            });
        };

        if ($state.params.action == 'calificar') {
            if (!APP.user) {
                var url = '/rs/' + $state.params.slug;
                $window.location = url;
            } else
                model_load.then(loadRate);
        } else if ($state.params.action == 'calificaciones') {
            model_load.then(loadRatings);
        }
        $scope.action = $state.params.action || 'main';

        $scope.goRate = function() {
            var url = '/rs/' + $scope.slug + '/calificar';
            if (!APP.user) {
                $scope.showLogin(url);
            } else {
                //$scope.action = 'calificar';
                $state.go('rs-action', { slug: $scope.slug, action: 'calificar' });
                //$location.path(url).state({ rs: $scope.rs });
            }
        };

        $scope.goRatings = function() {
            //var url = '/rs/' + $scope.slug + '/calificaciones';
            //$scope.action = 'calificaciones';
            $state.go('rs-action', { slug: $scope.slug, action: 'calificaciones' });
        };

        $scope.submitExperience = function() {
            $scope.errorSubmitExperience = '';
            if ($scope.rateForm.score < 1) {
                $scope.errorSubmitExperience = 'Debe calificar con estrellas';
                return;
            }
            if ($scope.rateForm.content.length < 2) {
                $scope.errorSubmitExperience = 'Debe describir alguna experiencia de uso';
                return;
            }
            var activities_ids = [];
            angular.forEach($scope.rateForm.activities, function(a, k) {
                if (a) activities_ids.push(k);
            });
            if (activities_ids.length < 1) {
                $scope.errorSubmitExperience = 'Debe seleccionar al menos un tipo de actividad';
                return;
            }
            var params = {
                rs_id: $scope.rs._id,
                score: $scope.rateForm.score,
                content: $scope.rateForm.content,
                activities_ids: activities_ids,
            };
            var success = function(res) {
                if (res.data.success) {
                    $scope.rateForm._id = res.data.success;
                    $scope.goRatings();
                }
            };
            if ($scope.rateForm._id)
                $http.put('/api/rate/' + $scope.rateForm._id, params).then(success);
            else
                $http.post('/api/rate', params).then(success);

        };

        $scope.removeRS = function(item) {
            if (confirm("¿Realmente desea eliminar este elemento?\nNo podrá recuperarlo después.")) {
                $http.delete('/api/resource_service/' + item._id).then(function() {
                    $state.go('my-suggestions')
                });

            }
        };

        $scope.removeRate = function(rate) {
            if (confirm("¿Realmente desea eliminar esta calificación?")) {
                $http.delete('/api/rate/' + rate._id).then(function() {
                    $scope.ratings.splice($scope.ratings.indexOf(rate), 1);
                });
            }
        }
    }
]);