app.controller('LoginController', ['$scope', '$animate', 'localStorageService', '$window', '$http', '$alert', '$timeout', 'Upload', 'APP', 'urlToGo',
    function($scope, $animate, localStorageService, $window, $http, $alert, $timeout, Upload, APP, urlToGo) {

        var conf = $scope.conf = {
            logging: false,
            busy: false,
            error: '',
        };

        var form_login = $scope.form_login = {
            username: "",
            password: ""
        };

        $scope.login = function() {
            if (!form_login.username || !form_login.password) return;
            conf.logging = conf.busy = true;
            conf.error = '';
            $http.post('/api/login', form_login).then(function(success) {
                if (success.data.success) {
                    if (urlToGo)
                        $window.location = urlToGo;
                    else
                        $window.location.reload();
                }
            }, function(error) {
                conf.logging = conf.busy = false;
                conf.error = "La contraseña es incorrecta. Vuelve a intentarlo.";
            }).finally(function() {

            });

            console.log($scope.form_login);
        };
    }
]);