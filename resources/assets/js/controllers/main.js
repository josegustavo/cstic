app.controller('MainController', ['$scope', '$animate', 'localStorageService', '$location', '$http', '$alert', '$timeout', 'Upload', 'APP', '$filter', '$window', '$modal', '$sce',
    function($scope, $animate, localStorageService, $location, $http, $alert, $timeout, Upload, APP, $filter, $window, $modal, $sce) {

        console.log('execure MainController');

        /*
                if (typeof(browser_old) == "undefined") {
                    initRipplesWithArrive();

                    $(document).arrive('.navbar-toggle', function() {
                        $(this).sideNav({ menuWidth: 260, closeOnClick: true });
                    });
                }
        */
        if (!localStorageService.get(APP.refresh)) {
            localStorageService.clearAll();
            localStorageService.set(APP.refresh, true);
        }

        $scope.showLogin = function(urlToGo) {
            var myOtherModal = $modal({
                scope: $scope,
                templateUrl: APP.cdn_url+'/assets/tpl/login.html?' + APP.refresh,
                show: false,
                animation: 'am-fade-and-slide-top',
                container: 'body',
                controller: "LoginController",
                resolve: {
                    urlToGo: function() { return urlToGo; }
                }
            });
            myOtherModal.$promise.then(myOtherModal.show);
            return myOtherModal;
        };

        $scope.logout = function() {
            $http.post('/api/logout').finally(function() {
                $window.location.reload();
            });
        };

        if (!localStorageService.get('types')) {
            $http.get('/api/type').then(function(res) {
                $scope.types = res.data;
                localStorageService.set('types', res.data);
            });
        } else {
            $scope.types = localStorageService.get('types');
        }
        var types_by_id = null;
        $scope.getType = function(id) {
            if (types_by_id === null && $scope.types) {
                types_by_id = {};
                angular.forEach($scope.types, function(a) {
                    types_by_id[a._id] = a;
                });
            }
            return types_by_id ? types_by_id[id] : null;
        };

        if (!localStorageService.get('activities')) {
            $http.get('/api/activity').then(function(res) {
                $scope.activities = res.data;
                localStorageService.set('activities', res.data);
            });
        } else {
            $scope.activities = localStorageService.get('activities');
        }
        var activities_by_id = null;
        $scope.getActivity = function(id) {
            if (!activities_by_id) {
                activities_by_id = {};
                angular.forEach($scope.activities, function(a) {
                    activities_by_id[a._id] = a;
                });
            }
            return activities_by_id[id];
        };

        if (!localStorageService.get('suppliers')) {
            $http.get('/api/supplier').then(function(res) {
                $scope.suppliers = res.data;
                localStorageService.set('suppliers', res.data);
            });
        } else {
            $scope.suppliers = localStorageService.get('suppliers');
        }
        var suppliers_by_id = null;
        $scope.getSupplier = function(id) {
            if (!suppliers_by_id && $scope.suppliers) {
                suppliers_by_id = {};
                angular.forEach($scope.suppliers, function(a) {
                    suppliers_by_id[a._id] = a;
                });
            }
            return suppliers_by_id ? suppliers_by_id[id] : null;
        };

        var departments_by_id = null;
        $scope.getDepartment = function(id) {
            if (departments_by_id === null) {
                departments_by_id = {};
                angular.forEach($scope.suppliers, function(a) {
                    angular.forEach(a.departments, function(d) {
                        departments_by_id[d._id] = d;
                    });
                });
            }
            return departments_by_id[id];
        };

        $scope.resetSearchConfig = function() {
            $scope.search_config = {
                order_asc: false,
                order_by_index: 0,
                block_advance_search: {
                    active: true,
                },
                block_activities: {
                    collapsed: false
                },
                block_types: {
                    collapsed: false
                },
                block_suppliers: {
                    collapsed: false,
                    include_suggested: true,
                    suppliers: {}
                }
            };
            return $scope.search_config;
        };
        if (!localStorageService.get('search_config')) {
            var search_config = $scope.resetSearchConfig();
            localStorageService.set('search_config', search_config);
        }
        localStorageService.bind($scope, 'search_config');


        $scope.getSelectedSupplierText = function(supplier) {
            var count = ($scope.search_config.block_suppliers.suppliers && $scope.search_config.block_suppliers.suppliers[supplier._id]) ? $scope.search_config.block_suppliers.suppliers[supplier._id].departments_ids.length : 0;

            var other_checkeds = 0;
            angular.forEach($scope.search_config.block_suppliers.suppliers, function(s, i) {
                if (i != supplier._id && s.checked) other_checkeds++;
            });

            if (count == 0 && other_checkeds && (!$scope.search_config.block_suppliers.suppliers || !$scope.search_config.block_suppliers.suppliers[supplier._id] || !$scope.search_config.block_suppliers.suppliers[supplier._id].checked)) {
                return "No se incluirá en la búsqueda";
            } else if (count == 0 || supplier.departments.length == count)
                return "Se buscará en " + supplier.description.split(',')[3];
            else
                return "Se buscará en " + count + " " + (supplier.description.split(',')[count > 1 ? 1 : 0]);
        };

        $scope.toggleSelectSupplier = function(i) {
            if (!$scope.search_config.block_suppliers.suppliers[i._id] || !$scope.search_config.block_suppliers.suppliers[i._id].checked) {
                $scope.search_config.block_suppliers.suppliers[i._id] = { checked: true, departments_ids: [] };
            } else {
                $scope.search_config.block_suppliers.suppliers[i._id].checked = false;
                $scope.search_config.block_suppliers.suppliers[i._id].departments_ids = [];
            }

        }

        $scope.getSelectedTypeText = function() {
            var count = 0;
            angular.forEach($scope.search_config.block_types.types, function(type, type_id) {
                if (type.checked) count++;
            });
            if (count == 0 || $scope.types.length == count)
                return "Se buscará en todos";
            else
                return count + " seleccionado" + (count > 1 ? 's' : '');
        };

        $scope.getSelectedActivityText = function() {
            var count = 0;
            angular.forEach($scope.search_config.block_activities.activities, function(activity, activity_id) {
                if (activity.checked) count++;
            });
            if (count == 0 || $scope.activities.length == count)
                return "Se buscará en todas";
            else
                return count + " seleccionada" + (count > 1 ? 's' : '');
        };


        $scope.local_config = {
            search_text: '',
        };


        $scope.goToSearch = function(text) {
            if (text)
                $scope.local_config.search_text = text;
            //if ($scope.local_config.search_text)
            $location.url('/buscar/' + $scope.local_config.search_text);
        };


        $scope.uploadImage = function(successCallback, errorCallback, progressCallback) {

            var file = document.createElement("input");
            file.setAttribute('type', 'file');
            file.setAttribute('accept', 'image/*');

            file.onchange = function(e) {
                if (!file || !file.files || !file.files[0]) return;
                console.log(e);
                return Upload.upload({
                    url: '/api/file',
                    file: file.files[0],

                }).then(
                    function(result) {
                        var data = result.data;
                        if (successCallback) successCallback(data);
                    }, errorCallback, progressCallback);
            };
            file.click();
        };

        $scope.theme_colors = [
            'pink', 'red', 'purple', 'indigo', 'blue',
            'light-blue', 'cyan', 'teal', 'green', 'light-green',
            'lime', 'yellow', 'amber', 'orange', 'deep-orange'
        ];

        /*
        // Add todoService to scope
        service = new todoService($scope);
        $scope.todosCount = service.count();
        $scope.$on('todos:count', function(event, count) {
            $scope.todosCount = count;
            element = angular.element('#todosCount');

            if (!element.hasClass('animated')) {
                $animate.addClass(element, 'animated bounce', function() {
                    $animate.removeClass(element, 'animated bounce');
                });
            }
        });
        */

        $scope.fillinContent = function() {
            $scope.htmlContent = 'content content';
        };

        // theme changing
        $scope.changeColorTheme = function(cls) {
            $scope.theme.color = cls;
        };

        $scope.changeTemplateTheme = function(cls) {
            $scope.theme.template = cls;
        };

        if (!localStorageService.get('theme')) {
            theme = {
                color: 'theme-blue',
                template: 'theme-template-dark'
            };
            localStorageService.set('theme', theme);
        }
        localStorageService.bind($scope, 'theme');
        /*
               var introductionAlert = $alert({
                   title: 'Welcome to Materialism',
                   content: 'Stay a while and listen',
                   placement: 'top',
                   type: 'theme',
                   show: false,
                   template: 'assets/tpl/partials/alert-introduction.html',
                   animation: 'mat-grow-top-right'
               });

              
               if (!localStorageService.get('alert-introduction')) {
                   $timeout(function() {
                       $scope.showIntroduction();
                       localStorageService.set('alert-introduction', 1);
                   }, 1750);
               }

               $scope.showIntroduction = function() {
                   introductionAlert.show();
               };
             */


        $scope.getUrlSecure = function(url) {
            return $sce.trustAsResourceUrl(url);
        }



    }
]);