app.controller('RsEditController', [
    '$window', '$scope', '$rootScope', '$aside', '$interval', '$http', '$location', '$state', 'APP', '$q', 'localStorageService', '$sce',

    function($window, $scope, $rootScope, $aside, $interval, $http, $location, $state, APP, $q, localStorageService, $sce) {

        console.log('Run RsEditController', $state);

        $rootScope.pageTitle = 'Modificar Recurso o Serivicio';
        $rootScope.hideLogo = false;

        if (!APP.user) {
            var login_modal = $scope.showLogin();
        }

        var slug = $state.params.slug;
        var conf = $scope.conf = {};

        var upload = $scope.uploadImage;
        $scope.uploadImage = function() {
            upload(function(data) {
                $scope.rs.image = data;
                $scope.rs.image_id = data._id;
            });
        };

        $http.get('/api/feature').then(function(res) {
            $scope.features = res.data;
        });

        var model_load = $q(function(resolve, reject) {

            if (slug) {
                $http.get('/api/resource_service/' + slug).then(function(res) {
                    $scope.rs = res.data;
                    $scope.rs.type = $scope.getType($scope.rs.type_id);
                    $scope.rs.supplier = $scope.getSupplier($scope.rs.supplier_id);
                    resolve($scope.rs);
                });
            } else {
                if (!localStorageService.get('rs')) {
                    localStorageService.set('rs', {});
                }
                localStorageService.bind($scope, 'rs');
                resolve($scope.rs);
            }

        });
        model_load.then();

        $scope.addActivity = function(activity_id) {
            if (!angular.isArray($scope.rs.rs_activities)) {
                $scope.rs.rs_activities = [{ activity_id: activity_id, details: [''] }];
            } else {
                var exist = -1;
                angular.forEach($scope.rs.rs_activities, function(rs_activity, index) {
                    if (rs_activity.activity_id == activity_id)
                        exist = index;
                });
                if (exist >= 0) {
                    $scope.rs.rs_activities[exist].details.push('');
                } else {
                    $scope.rs.rs_activities.push({ activity_id: activity_id, details: [''] });
                }
            }
        };

        var getDataToSubmit = function() {
            var rs = $scope.rs;

            var activities_obj = {};
            //Actividades
            angular.forEach(rs.rs_activities, function(a) {
                if (a.details && a.details.length) {
                    if (!activities_obj[a.activity_id]) {
                        activities_obj[a.activity_id] = {
                            activity_id: a.activity_id,
                            details: a.details
                        };
                    } else {
                        activities_obj[a.activity_id].details.push(a.details[0]);
                    }
                }
            });
            var activities = [];
            angular.forEach(activities_obj, function(a) {
                activities.push(a);
            });

            var features = [];
            angular.forEach(rs.rs_features, function(f) {
                features.push(f);
            });
            /*var search = angular.copy(rs.tags);
            search.push(rs.name);
            search.push(rs.description);*/

            //Preparar la data a enviar
            var data = {
                name: rs.name,
                description: rs.description,
                supplier_id: rs.supplier_id,
                tags: rs.tags,
                department_id: rs.department_id,
                type_id: rs.type_id,
                //type: { name: $scope.types[rs.type_id].name },
                rs_activities: activities,
                rs_features: features,
                image_id: rs.image ? rs.image._id : null,
            };

            return data;
        };




        $scope.save = function() {
            conf.saving = true;
            var data = getDataToSubmit();

            var success = function(resp) {};
            var error = function(err) {};
            if (slug) {
                $http.put('/api/resource_service/' + $scope.rs._id, data).then(
                    function(resp) {
                        $state.go('rs', { slug: resp.data.slug })
                    },
                    function(err) {
                        console.error(err);
                    }).finally(function() {
                    conf.saving = false;
                });
            } else {
                $http.post('/api/resource_service', data).then(
                    function(resp) {
                        localStorageService.set('rs', {});
                        $state.go('rs', { slug: resp.data.slug })
                    },
                    function(err) {

                    });
            }
        };

        $scope.cancel = function() {
            if (slug) {
                localStorageService.set('rs', {});
            }
            $window.history.back();
        };


        $scope.loadUrl = function(rs_feature) {
            var url = rs_feature.content;
            if (/^(?:(?:(?:https?):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(url)) {
                $http.get('/api/service/url_card', { params: { url: url } }).then(function(res) {
                    rs_feature.url_data = res.data;
                }, function() {
                    rs_feature.url_data = null;
                });
            } else {
                rs_feature.url_data = null;
            }
        }


        $scope.loadUrlVideo = function(rs_feature) {
            rs_feature.url_data = null;
            if (!rs_feature.content) return;
            var regex_youtube = /^(?:https?\:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.?be)\/.+v=(.+)$/;
            if ((m = regex_youtube.exec(rs_feature.content)) !== null) {
                if (m[1]) {
                    rs_feature.url_data = { type: 'youtube', v: m[1] };
                }
            }
        }
    }
]);