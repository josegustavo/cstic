module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);
  
  var pkg = grunt.file.readJSON('package.json');
	
  grunt.initConfig({
	
    appConfig: {
      path: 'resources/assets/',
      assets: 'public/assets/',
      dist: '',
      distAssets: '',
      index: 'resources/views/index.blade.php',
      indexDev: 'resources/views/index.dev.blade.php'
    },

    // version update
    bump: {
      options: {
        files: ['package.json', 'bower.json']
      }
    },

    // application constants
    

    // clean generated files
    clean: {
      release: [
        '<%= appConfig.assets %>'
      ]
    },

    // copy development file as distribution
    copy:{
      fonts: {
        expand: true,
        src: [
          '<%= appConfig.path %>bower/font-awesome/fonts/*',
          '<%= appConfig.path %>bower/material-design-iconic-font/fonts/*',
          '<%= appConfig.path %>bower/roboto-fontface/fonts/*',
          '<%= appConfig.path %>bower/weather-icons/font/*',
          '<%= appConfig.path %>bower/bootstrap-sass/assets/fonts/bootstrap/*'
        ],
        dest: '<%= appConfig.assets %>fonts/',
        filter: 'isFile',
        flatten: true
      },
      html: {
        src: '<%= appConfig.indexDev %>',
        dest: '<%= appConfig.index %>'
      },
      //pages: {
      //  expand: true,
      //  cwd: 'materialism/pages',
      //  src: ['**/*'],
      //  dest: '<%= appConfig.dist %>/pages'
      //},
	  img : {
		expand: true,
		cwd: '<%= appConfig.path %>img',
		src: ['**/*'],
        dest: '<%= appConfig.assets %>img'
	  },
      fontsdist: {
        expand: true,
        cwd: '<%= appConfig.path %>fonts',
        src: ['**/*'],
        dest: '<%= appConfig.assets %>fonts'
      },
      //tpldist: {
      //  expand: true,
      //  cwd: '<%= appConfig.path %>/tpl',
      //  src: ['**/*'],
      //  dest: '<%= appConfig.assets %>/tpl'
      //}
    },
	htmlmin: {                                     // Task
		dist: {                                      // Target
			options: {                                 // Target options
				removeComments: true,
				collapseWhitespace: true
			},
			files: [{
				expand: true,
				cwd: '<%= appConfig.path %>/tpl',
				src: ['**/*.html'],
				dest: '<%= appConfig.assets %>/tpl'
			}]
		}
	},
    // set usemin working file
    useminPrepare: {
      html: '<%= appConfig.index %>',
      options: {
        dest: 'public/',
        root: '<%= appConfig.path %>'
      }
    },

    // sass our development files into 1 stylesheet
    sass: {
      dist: {
        options: {	
          style: 'expanded'
        },
        files: {
          '<%= appConfig.path %>css/materialism.css': '<%= appConfig.path %>css/sass/materialism.scss'
        }
      }
    },

    // remove all bs from css
    cssmin: {
      options: {
        keepSpecialComments: 0
      }
    },

    // optimize images
    imagemin : {
      dynamic : {
		  options: {
                optimizationLevel: 3,
                progressive: true,
              
            },
        files : [{
          expand : true,
          cwd : '<%= appConfig.path %>img/', // source images (not compressed)
          src : ['**/*.{png,jpg,gif,svg,xml,json,ico}'], // Actual patterns to match
          dest : '<%= appConfig.assets %>img/' // Destination of compressed files
        }]
      }
    },

    // add rev to bust cache
    filerev: {
      options: {
        encoding: 'utf8',
        algorithm: 'md5',
        length: 20
      },
      release: {
        src: [
          '<%= appConfig.assets %>**/*.js',
          '<%= appConfig.assets %>**/*.css',
        ]
      }
    },
	
	replace: {
	   dist : {
		   src: ['<%= appConfig.index %>'],
		overwrite: true,                 // overwrite matched source files 
		replacements: [{
		  from: "=\"/assets",
		  to: "=\"{{config('app.cdn_url')}}/assets"
		}]
	
	  }
	},


    // replace tags
    usemin:{
      html: '<%= appConfig.index %>',
      options: {
        assetsDirs: 'public/',
        patterns: {
          html: [
            [
              /(<!-- reusebuild:css .+? -->[\s\S\r\n]*?<!-- endreusebuild -->)/gm,
              'Re-use css build',
              function (m) {
                return m.match(/[\/.a-z]*?\.css/gm)[0];
              },
              function (m) {
                return '<link href="'+m+'" rel="stylesheet" />';
              }
            ],
            [
              /(<!-- reusebuild:js .+? -->[\s\S\r\n]*?<!-- endreusebuild -->)/gm,
              'Re-use js build',
              function (m) {
                return m.match(/[\/.a-z]*?\.js/gm)[0];
              },
              function (m) {
                return '<script charset="utf-8" src="'+m+'"></script>';
              }
            ]
          ]
        },
      }
    },

	
	concat: {
	  options: {
		sourceMap: false
	  }
	},

	uglify: {
	  options: {
		sourceMap: false,
		sourceMapIn: function(uglifySource) {
		  return uglifySource + '.map';
		},
		sourceMapIncludeSources: true
	  }
	},

    // development
    watch: {
      js: {
        files: ['Gruntfile.js', '<%= appConfig.path %>js/**/*.js'],
        tasks: ['jshint'],
        options: {
          livereload: true
        }
      },
      css: {
        files: [
          '<%= appConfig.path %>css/**/*.scss'
        ],
        tasks: ['sass'],
        options: {
          livereload: true
        }
      }
    },

    // debug while developing
    jshint: {
      all: ['Gruntfile.js', '<%= appConfig.path %>js/**/*.js']
    },

  });

  grunt.registerTask('version', ['bump-only']);
  grunt.registerTask('css', ['sass','watch']);
  grunt.registerTask('dev', ['sass', 'copy', 'watch']);
  grunt.registerTask('rep', ['replace']);

  grunt.registerTask('default',[
    'clean',
    'sass',
    'copy',
	'htmlmin',
    'useminPrepare',
    'concat',
    'cssmin',
    'uglify',
    'filerev',
    'usemin',
	'replace',
    //'imagemin',
  ]);

};






